package com.itau.oauth.agenda.services;

import com.itau.oauth.agenda.models.Agenda;
import com.itau.oauth.agenda.repositories.AgendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

@Service
public class AgendaService {

    @Autowired
    private AgendaRepository agendaRepository;

    @ResponseStatus(HttpStatus.CREATED)
    public Agenda salvarContato(Agenda agenda) {
        return agendaRepository.save(agenda);
    }

    public Iterable<Agenda> buscarTodosContatosUsuario(int usuarioId) {
        Iterable<Agenda> agendas = agendaRepository.findByUsuarioId(usuarioId);
        return agendas;
    }
}
