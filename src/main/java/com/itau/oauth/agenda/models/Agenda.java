package com.itau.oauth.agenda.models;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class Agenda {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min=1, max=3, message = "DDD do telefone deve ter entre 1 a 3 caracteres")
    private String ddd;

    @Size(min=3, max=9, message = "Número de telefone deve ter entre 3 a 9 caracteres")
    private String telefone;

    private int usuarioId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
