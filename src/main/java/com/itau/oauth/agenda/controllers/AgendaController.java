package com.itau.oauth.agenda.controllers;

import com.itau.oauth.agenda.models.Agenda;
import com.itau.oauth.agenda.security.Usuario;
import com.itau.oauth.agenda.services.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class AgendaController {

    @Autowired
    private AgendaService agendaService;

    @PostMapping("/contato")
    public Agenda create(@RequestBody Agenda agenda, @AuthenticationPrincipal Usuario usuario) {
            agenda.setUsuarioId(usuario.getId());
            agenda = agendaService.salvarContato(agenda);
            return agenda;
    }

    @GetMapping("/contatos")
    @ResponseStatus(code= HttpStatus.OK)
    public Iterable<Agenda> buscarTodos(@AuthenticationPrincipal Usuario usuario) {
        return agendaService.buscarTodosContatosUsuario(usuario.getId());
    }
}