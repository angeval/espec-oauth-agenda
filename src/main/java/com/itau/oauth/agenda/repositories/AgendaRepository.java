package com.itau.oauth.agenda.repositories;

import com.itau.oauth.agenda.models.Agenda;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface AgendaRepository extends CrudRepository<Agenda, Integer> {
    List<Agenda> findByUsuarioId(int usuarioId);
}
